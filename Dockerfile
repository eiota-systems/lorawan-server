FROM erlang:21-alpine
MAINTAINER Petr Gotthard <petr.gotthard@centrum.cz>

WORKDIR /app

RUN apk add --no-cache --virtual build-deps git make wget nodejs-npm
COPY . .
RUN make release install

# volume for the mnesia database and logs
RUN mkdir /storage
VOLUME /storage

# data from port_forwarders
EXPOSE 1680/udp
# http admin interface
EXPOSE 8080/tcp
# https admin interface
EXPOSE 8443/tcp

ENV LORAWAN_HOME=/storage
WORKDIR /usr/lib/lorawan-server
CMD bin/lorawan-server
